# Producst api

This is the JoyJet App test

to start the project:

    $ python3 -m venv venv #inside the folder

    $ source /venv/bin/activate

    $ pip install -r requirements.txt

    $ ./manage.py makemigrations && ./manage.py migrate && ./manage.py createsuperuser

    $./manage.py runserver


to view the project working you can access:

  http://localhost:8000/admin

  http://localhost:8000/articles

  http://localhost:8000/cart