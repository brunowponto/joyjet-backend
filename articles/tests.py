from django.test import TestCase
from .models import Article

# class ArticleTestCase(TestCase):
#     def setUp(self):
#         Article.objects.create(name='fries', price='378')
#         Article.objects.create(name='ham', price='147')
#         Article.objects.create(name='water', price='100')
#         Article.objects.create(name='honey', price='200')
#         Article.objects.create(name='mango', price='400')
#         Article.objects.create(name='tea', price='1000')
#         Article.objects.create(name='ketchup', price='999')

from rest_framework.test import APIRequestFactory

factory = APIRequestFactory()
request = factory.post('/articles/', {'name' : 'water', 'price' : '100'}, format='json')
request = factory.post('/articles/', {'name' : 'honey', 'price' : '200'}, format='json')
request = factory.post('/articles/', {'name' : 'mango', 'price' : '400'}, format='json')
