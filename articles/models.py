from django.db import models

class Article(models.Model):
    name = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=5 , decimal_places=2)

    class Meta:
        ordering = ['-name']
        verbose_name = 'Product'
        verbose_name_plural = 'Products'

    def __str__(self):
        return self.name
