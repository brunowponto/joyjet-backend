from django.contrib import admin
from django.urls import path, include
# from rest_framework_swagger.views import get_swagger_view

from articles.views import articles_list, article_detail
from cart.views import  carts_list, cart_detail

# schema_view = get_swagger_view(title='Articles')

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('swagger/', schema_view, name='schema_view'),
    path('articles/', articles_list, name='articles_list'),
    path('articles/<int:pk>', article_detail, name='article_detail'),
    path('cart/', carts_list, name='cart_list'),
    path('cart/<int:pk>', cart_detail, name='cart_detail'),
]
