from django.db import models
from articles.models import Article
from django.contrib.auth.models import User


class Cart(models.Model):
    article_id = models.ForeignKey(Article, on_delete='cascade', related_name='article')
    user_id = models.ForeignKey(User, on_delete='cascade', related_name='user')
    quantity = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return self.article_id.name



