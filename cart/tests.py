# from django.test import TestCase
from .models import Cart
from rest_framework.test import APIRequestFactory

factory = APIRequestFactory()
request = factory.post('/cart/', {'article_id' : '1', 'user_id': '1', 'quantity': '6'}, format='json')
request = factory.post('/cart/', {'article_id' : '2', 'user_id': '1', 'quantity': '10'}, format='json')
